import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('/Users/bole/Desktop/ISTA131/Final/fav_cereals.csv')

x = df['name'].tolist()
y = df['sodium'].tolist()

bar_colors = ['#72DE3C', '#E9CA2B', '#D1A05F', '#BD8232', '#5A3F1C', '#E8E81E', '#FF0303', 
'#F4C246', '#FFFFFF', '#FF0303', '#EDC71B', '#ED771B', '#FF0303', '#D1A05F', '#D78559', '#A321DF', 
'#3821DF', '#FD0101', '#FF0303', '#F4760E']

plt.bar(x, y, 0.6, color = bar_colors)
plt.title('Amount of sodium in favorite cereals')
plt.xlabel('Cereals')
plt.ylabel('Sodium(mg)')
plt.xticks(rotation = 90)

plt.show()