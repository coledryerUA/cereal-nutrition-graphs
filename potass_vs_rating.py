import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


df = pd.read_csv('/Users/bole/Desktop/ISTA131/Final/cereal.csv')

x = df['potass'].tolist()
y = df['rating'].tolist()

p = np.array(x)
r = np.array(y)

plt.plot(p, r, 'o')
sns.regplot(p, r, ci=None)
plt.title('Potassium vs Customer Rating')
plt.xlabel('Potassium(mg)')
plt.ylabel('Customer Rating')
plt.show()